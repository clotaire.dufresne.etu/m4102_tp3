| URI               | Opération | MIME                                                        | Requête    | Réponse                                                                                       |
|-------------------|-----------|-------------------------------------------------------------|------------|-----------------------------------------------------------------------------------------------|
| /pizzas           | GET       | <- application/json <- application/xml                      |            | Liste des pizzas existantes                                                                   |
| /pizzas/{id}      | GET       | <- application/json <- application/xml                      |            | La pizza {id} ou 404                                                                          |
| /pizzas/{id}/name | GET       | <- text/plain                                               |            | Le nom de la pizza {id} ou 404                                                                |
| /pizzas           | POST      | <-/-> application/json -> application/x-www-form-urlencoded | Pizza (P1) | La nouvelle pizza (P2) ou 409 si l'ingrédient existe déjà ou 406 si le JSON ne correspond pas |
| /pizzas/{id}      | DELETE    |                                                             |            |                                                                                               |



### Si vous souhaitez lancer les tests sans la variable d'environnement "pizzaenv" pensez à décommenter les quelques lignes en commentaire dans PizzaResourceTest