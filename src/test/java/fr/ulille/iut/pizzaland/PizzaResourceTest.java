package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.json.Json;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest {

	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
	private PizzaDao daoPizza;
	private IngredientDao daoIngredient;
	private List<Ingredient> ingredients;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		daoPizza = BDDFactory.buildDao(PizzaDao.class);
		daoIngredient= BDDFactory.buildDao(IngredientDao.class);

		daoIngredient.createTable();
		daoPizza.createTableAndIngredientAssociation();
		
		
		//A DECOMMENTER SI VOUS N'UTILISEZ PAS LA VARIABLE D'ENVIRONNEMENT 'WITHDB'
//		Ingredient tomate = new Ingredient("tomate");
//		Ingredient ananas = new Ingredient("ananas");
//		
//		daoIngredient.insert(tomate);
//		daoIngredient.insert(ananas);

		ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("ananas"));
		ingredients.add(daoIngredient.findByName("tomate"));

	}

	@After
	public void tearEnvDown() throws Exception {
		daoIngredient.dropTable();
		daoPizza.dropAll();
	}

	@Test
	public void testGetEmptyList() {
		daoPizza.dropAll();
		daoPizza.createTableAndIngredientAssociation();
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, pizzas.size());
	}

	@Test
	public void testGetExistingPizza() {

		Pizza pizza = new Pizza();
		pizza.setName("pizzaChorizo");
		pizza.setIngredients(ingredients);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		LOGGER.info("Ingredients : "+ pizza.getIngredients());
		LOGGER.info("Ingredients 2 : "+ result.getIngredients());
		assertEquals(pizza, result);
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testGetExistingPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("pizzaAnanas");
		pizza.setIngredients(ingredients);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals(pizza.getName(), response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("tomateAnanas");
		pizzaCreateDto.setIngredients(ingredients);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
		assertEquals(pizzaCreateDto.getIngredients(), returnedEntity.getIngredients());
	}

	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza();
		pizza.setName("tomateAnanas");
		pizza.setIngredients(ingredients);
		daoPizza.insert(pizza);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setIngredients(ingredients);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutIngredients() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Un nom valide");

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreatePizzaWithWrongIngredient() {
		ArrayList<Ingredient> wrongIngredients = new ArrayList<>();
		wrongIngredients.add(new Ingredient("Not in DB"));
		
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("un nom");
		pizzaCreateDto.setIngredients(wrongIngredients);

		Response response = target("pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateWithDoubleIngredients() {
		List<Ingredient> doubleIngredients = new ArrayList<>();
		doubleIngredients.add(daoIngredient.findByName("tomate"));
		doubleIngredients.add(daoIngredient.findByName("tomate"));
		
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Une super pizza");
		pizzaCreateDto.setIngredients(doubleIngredients);
		
		Response response = target("pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateWithForm() {		
		Form form = new Form();
		form.param("name", "tomateChorizo");
		form.param("idIngredients", ingredients.get(0).getId().toString());
		form.param("idIngredients", ingredients.get(1).getId().toString());

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("/pizzas").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = daoPizza.findById(UUID.fromString(id));

		
		assertNotNull(result);
		assertTrue(!result.getIngredients().isEmpty());
		assertTrue(result.getIngredients().contains(ingredients.get(0)));
		assertTrue(result.getIngredients().contains(ingredients.get(1)));
		assertEquals(result.getIngredients().size(), ingredients.size());
	}

	@Test
	public void testCreateWithJson() {
		JsonObjectBuilder tomate = Json.createObjectBuilder().add("name", ingredients.get(0).getName());
		tomate.add("id", ingredients.get(0).getId().toString());
		JsonObjectBuilder ananas = Json.createObjectBuilder().add("name", ingredients.get(1).getName());
		ananas.add("id", ingredients.get(1).getId().toString());
		
		JsonArrayBuilder ingredientsJson = Json.createArrayBuilder().add(tomate);
		ingredientsJson.add(ananas);
		
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder().add("name", "tomateAnanas");
		jsonBuilder.add("ingredients", ingredientsJson);
		JsonObject json = jsonBuilder.build();
				
		Entity<JsonObject> jsonEntity = Entity.entity(json, MediaType.APPLICATION_JSON);
		Response response = target("/pizzas").request().post(jsonEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = daoPizza.findById(UUID.fromString(id));

		assertNotNull(result);
		assertTrue(result.getIngredients().contains(ingredients.get(0)));
		assertTrue(result.getIngredients().contains(ingredients.get(1)));
		assertEquals(result.getIngredients().size(), ingredients.size());
	}
	
	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		pizza.setIngredients(ingredients);
		daoPizza.insert(pizza);
		
		Response response = target("/pizzas").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = daoPizza.findById(pizza.getId());
		assertEquals(result, null);
		
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

}
