package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.json.Json;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeResourceTest extends JerseyTest {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
	private PizzaDao daoPizza;
	private IngredientDao daoIngredient;
	private CommandeDao daoCommande;
	private List<Ingredient> ingredients;
	private List<Pizza> pizzas;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		daoPizza = BDDFactory.buildDao(PizzaDao.class);
		daoIngredient= BDDFactory.buildDao(IngredientDao.class);
		daoCommande = BDDFactory.buildDao(CommandeDao.class);
		
		daoIngredient.createTable();
		daoPizza.createTableAndIngredientAssociation();
		daoCommande.createTableAndPizzaAssociation();
		
		//A DECOMMENTER SI VOUS N'UTILISEZ PAS LA VARIABLE D'ENVIRONNEMENT 'WITHDB'
//		Ingredient tomate = new Ingredient("tomate");
//		Ingredient jambon = new Ingredient("jambon");
//		Ingredient mozzarella = new Ingredient("mozzarella");
//		
//		daoIngredient.insert(jambon);
//		daoIngredient.insert(tomate);
//		daoIngredient.insert(mozzarella);

		ingredients = new ArrayList<>();
		pizzas = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("mozzarella"));
		ingredients.add(daoIngredient.findByName("tomate"));
		ingredients.add(daoIngredient.findByName("jambon"));
		
		Pizza p = new Pizza("Reine");
		p.setIngredients(ingredients);
		
		Pizza p2 = new Pizza("Tomate");
		ArrayList<Ingredient> ingredientsBis = new ArrayList<>();
		ingredientsBis.add(daoIngredient.findByName("tomate"));
		p2.setIngredients(ingredientsBis);
		
		daoPizza.insert(p);
		daoPizza.insert(p2);
		
		pizzas.add(daoPizza.findById(p.getId()));
		pizzas.add(daoPizza.findById(p.getId()));
		pizzas.add(daoPizza.findById(p2.getId()));
	}

	@After
	public void tearEnvDown() throws Exception {
		daoCommande.dropAll();
		daoPizza.dropAll();
		daoIngredient.dropTable();
	}
	
	@Test
	public void testGetEmptyList() {
		daoCommande.dropAll();
		daoCommande.createTableAndPizzaAssociation();
		
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/commandes").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {
		});

		assertEquals(0, commandes.size());
	}
	
	@Test
	public void testGetExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Aldo Raine aka Aldo l'Apache");
		commande.setPizzas(pizzas);
		daoCommande.insert(commande);
		
		
		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);
	}

	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	
	@Test
	public void testGetExistingCommandeName() {
		Commande commande = new Commande();
		commande.setName("Donny Donowitz aka L'ours juif");
		commande.setPizzas(pizzas);
		daoCommande.insert(commande);

		Response response = target("/commandes").path(commande.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals(commande.getName(), response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingCommandeName() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	
	@Test
	public void testCreateCommande() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setName("Vincent Vega");
		commandeCreateDto.setPizzas(pizzas);

		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

		assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
		assertEquals(commandeCreateDto.getPizzas(), returnedEntity.getPizzas());
	}

	@Test
	public void testCreateSameCommande() {
		Commande commande = new Commande();
		commande.setName("Mia Wallace");
		commande.setPizzas(pizzas);
		daoCommande.insert(commande);

		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateCommandeWithoutName() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setPizzas(pizzas);

		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateCommandeWithoutPizzas() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setName("Un nom valide");

		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateCommandeWithWrongPizza() {
		ArrayList<Pizza> wrongPizzas = new ArrayList<>();
		wrongPizzas.add(new Pizza("Not in DB"));
		
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setPizzas(wrongPizzas);

		Response response = target("commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateWithJson() {
		Ingredient i = daoIngredient.findByName("tomate");
		Pizza p = daoPizza.findByName("Tomate");
		JsonObjectBuilder tomate = Json.createObjectBuilder().add("name", i.getName());
		tomate.add("id", i.getId().toString());
		
		JsonArrayBuilder ingredientsJson = Json.createArrayBuilder().add(tomate);
		
		JsonObjectBuilder pizza1 = Json.createObjectBuilder().add("name", p.getName()); //il aurait peut-être été plus pratique de faire une méthode qui récupère un json avec un paramètre "nombre" pour chaque pizza, mais il est trop tard pour tout changer :(
		pizza1.add("ingredients", ingredientsJson.build());
		pizza1.add("id", p.getId().toString());
		
		
		
		//l'objet ingredientsJson semblait "consommé" lors du premier ajout et ne mettait aucun ingrédient dans la pizza2, d'où le copié collé
		JsonObjectBuilder pizza2 = Json.createObjectBuilder().add("name", p.getName());
		
		tomate = Json.createObjectBuilder().add("name", i.getName());
		tomate.add("id", i.getId().toString());
		
		ingredientsJson.add(tomate);
		
		pizza2.add("ingredients", ingredientsJson.build());
		pizza2.add("id", p.getId().toString());
		
		JsonArrayBuilder pizzasJson = Json.createArrayBuilder().add(pizza1);
		pizzasJson.add(pizza2);
		
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder().add("name", "Jack Uzi");
		jsonBuilder.add("pizzas", pizzasJson);
		JsonObject json = jsonBuilder.build();
				
		Entity<JsonObject> jsonEntity = Entity.entity(json, MediaType.APPLICATION_JSON);
		Response response = target("/commandes").request().post(jsonEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Commande result = daoCommande.findById(UUID.fromString(id));

		List<Pizza> expectedPizzas = new ArrayList<>();
		expectedPizzas.add(p);
		expectedPizzas.add(p);
		
		assertNotNull(result);
		assertEquals(result.getPizzas(), expectedPizzas);
		assertEquals(result.getName(), "Jack Uzi");
	}
	
	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Shosanna Dreyfus");
		commande.setPizzas(pizzas);
		daoCommande.insert(commande);
		
		Response response = target("/commandes").path(commande.getId().toString()).request().delete();
		
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
		
		Commande result = daoCommande.findById(commande.getId());
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
}