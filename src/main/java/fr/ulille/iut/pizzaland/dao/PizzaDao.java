package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL);")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation ("
			+ "idPizza VARCHAR(128) NOT NULL, "
			+ "idIngredient VARCHAR(128) NOT NULL,"
			+ "PRIMARY KEY(idPizza, idIngredient),"
			+ "FOREIGN KEY(idPizza) REFERENCES Pizzas(id) ON UPDATE CASCADE ON DELETE CASCADE,"
			+ "FOREIGN KEY(idIngredient) REFERENCES Ingredients(id) ON UPDATE CASCADE ON DELETE CASCADE);")
	void createAssociationTable();

	@SqlUpdate("DROP TABLE IF EXISTS Pizzas;")
	void dropPizzas();

	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation;")
	void dropAssociationTable();


	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id;")
	void remove(@Bind("id") UUID id);

	@SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name);")
	void insertPizza(@BindBean Pizza pizza);

	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza,idIngredient) VALUES (:pizzaId, :ingredientId);")
	void insertAssociation(@Bind("pizzaId")UUID pizzaId, @Bind("ingredientId")UUID ingredientId);

	@SqlQuery("SELECT * FROM Pizzas;")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAllPizzas();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id;")
	@RegisterBeanMapper(Pizza.class)
	Pizza findPizzaById(@Bind("id")UUID id);

	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name;")
	@RegisterBeanMapper(Pizza.class)
	Pizza findPizzaByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM ingredients WHERE id IN (SELECT idIngredient FROM PizzaIngredientsAssociation WHERE idPizza = :idPizza);")
	@RegisterBeanMapper(Ingredient.class)
	List<Ingredient> getIngredients(@Bind("idPizza")UUID idPizza);
	
	

	@Transaction
	default void dropAll() {
		dropAssociationTable();
		dropPizzas();
	}

	@Transaction
	default void createTableAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
	}

	@Transaction
	default void insert(Pizza pizza) {
		insertPizza(pizza);
		for(Ingredient ingredient : pizza.getIngredients()) {
			insertAssociation(pizza.getId(), ingredient.getId());
		}
	}

	@Transaction
	default Pizza findById(UUID id) {
		Pizza pizza = findPizzaById(id);
		if(pizza != null) {
			pizza.setIngredients(getIngredients(pizza.getId()));
		}
		return pizza;
	}
	
	@Transaction
	default Pizza findByName(String name) {
		Pizza pizza = findPizzaByName(name);
		if(pizza != null) {
			pizza.setIngredients(getIngredients(pizza.getId()));
		}
		return pizza;
	}

	@Transaction
	default List<Pizza> getAll() {
		List<Pizza> pizzas = getAllPizzas();
		for(Pizza p : pizzas) {
			p.setIngredients(getIngredients(p.getId()));
		}
		return pizzas;
	}

}
