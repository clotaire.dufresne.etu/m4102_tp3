package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Path("/commandes")
public class CommandeResource {

	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());

	private CommandeDao commandes;
	private PizzaDao pizzas;
	private IngredientDao daoIngredients;

	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
		daoIngredients = BDDFactory.buildDao(IngredientDao.class);
		daoIngredients.createTable();

		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTableAndIngredientAssociation();

		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createTableAndPizzaAssociation();
	}

	@GET
	public List<CommandeDto> getAll() {
		LOGGER.info("CommandeRessource:getAll");

		List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@Path("{id}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
			Commande commande = commandes.findById(id);
			LOGGER.info(commande.toString());
			return Commande.toDto(commande);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("{id}/name")
	public String getCommandeName(@PathParam("id") UUID id) {
		Commande commande = commandes.findById(id);

		if (commande == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return commande.getName();
	}

	@POST
	public Response createPizza(CommandeCreateDto commandeCreateDto) {
		
		Commande existing = commandes.findByName(commandeCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		try {
			checkPizzas(commandeCreateDto.getPizzas());

			Commande commande= Commande.fromCommandeCreateDto(commandeCreateDto);
			if(commande.getPizzas() == null) {
				throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
			}
			commandes.insert(commande);
			CommandeDto pizzaDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path(commande.getId().toString()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}



	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createCommande(@FormParam("name") String name, @FormParam("idIngredients") List<String> idPizzas) {
		Commande existing = commandes.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		try {
			List<Pizza> pizzasCommande = checkPizzasId(idPizzas);

			Commande commande = new Commande();
			commande.setName(name);
			commande.setPizzas(pizzasCommande);

			commandes.insert(commande);

			CommandeDto commandeDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + commande.getId()).build();

			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if ( commandes.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		commandes.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	private List<Pizza> checkPizzasId(List<String> idPizzas) {
		// TODO Auto-generated method stub
		return null;
	}

	private void checkPizzas(List<Pizza> pizzasCommande) throws Exception {
		List<String> checkedIds = new ArrayList<>();
		if(pizzasCommande.isEmpty()) {
			throw new Exception("Liste d'ingrédients vide");
		}

		for(Pizza p : pizzasCommande) {
			if(pizzas.findById(p.getId()) == null) { //On évite les appels inutiles à la BDD
				throw new Exception();
			}
			checkedIds.add(p.getId().toString());
		}
	}

}
