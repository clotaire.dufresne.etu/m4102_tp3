package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());

	private PizzaDao pizzas;
	private IngredientDao daoIngredients;

	@Context
	public UriInfo uriInfo;

	public PizzaResource() {
		daoIngredients = BDDFactory.buildDao(IngredientDao.class);
		daoIngredients.createTable();

		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTableAndIngredientAssociation();
	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzaRessource:getAll");

		List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@Path("{id}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public PizzaDto getOnePizza(@PathParam("id") UUID id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);
			LOGGER.info(pizza.toString());
			return Pizza.toDto(pizza);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("{id}/name")
	public String getPizzaName(@PathParam("id") UUID id) {
		Pizza pizza = pizzas.findById(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getName();
	}

	@POST
	public Response createPizza(PizzaCreateDto pizzaCreateDto) {
		Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		try {
			checkIngredients(pizzaCreateDto.getIngredients());

			Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
			if(pizza.getIngredients() == null) {
				throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
			}
			pizzas.insert(pizza);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}


	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizza(@FormParam("name") String name, @FormParam("idIngredients") List<String> idIngredients) {
		Pizza existing = pizzas.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		try {
			List<Ingredient> ingredients = checkIngredientsId(idIngredients);

			Pizza pizza = new Pizza();
			pizza.setName(name);
			pizza.setIngredients(ingredients);

			pizzas.insert(pizza);

			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") UUID id) {
		if ( pizzas.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		pizzas.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	private List<Ingredient> checkIngredientsId(List<String> idIngredients) throws Exception{
		List<Ingredient> listeIngredients = new ArrayList<>();
		if(idIngredients.isEmpty()) {
			throw new Exception("Liste d'ingrédients vide");
		}

		for(String id : idIngredients) {
			Ingredient i = daoIngredients.findById(UUID.fromString(id));
			if(i == null) {
				throw new Exception("Ingrédient invalide");
			}
			listeIngredients.add(i);
		}
		return listeIngredients;
	}

	private void checkIngredients(List<Ingredient> ingredients) throws Exception {
		if(ingredients.isEmpty()) {
			throw new Exception("Liste d'ingrédients vide");
		}

		for(Ingredient i : ingredients) {
			if(daoIngredients.findById(i.getId()) == null) {
				throw new Exception("Ingrédient invalide");
			}
		}
	}

}

