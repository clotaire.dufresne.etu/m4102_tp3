package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {
	
	private UUID id;
	private String name;
	private List<Pizza> pizzas;

	public void setId(UUID id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}

	public UUID getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public List<Pizza> getPizzas() {
		return this.pizzas;
	}

}
